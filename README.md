#TeamRock Faux App

## Development Environment

To get up and running, you'll need VirtualBox and Vagrant installed. From the project root: vagrant up
This will boot an Ubuntu 14.04 virtual machine (192.168.66.66 with ports 3306 and 80 open) with two Docker containers: mysql and apache2

If you have any problems, feel free to separate the application folder and run on your own apache/mysql setup.

## Still to implement:

* Allow sorting of the News list by views, both ascending and descending
* Allow deleting, soft and hard, of News
* The content team have requested that News be weightable (Important, Semi Important, Useless)

## Bonus Points

* Instead of calling $news->onView(); inside the controller, try and move this to the service layer and call it using the event dispatcher