<?php

namespace TeamRock\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamRock\ContentBundle\Interfaces\DeletableInterface;
use TeamRock\ContentBundle\Interfaces\ViewableInterface;
use TeamRock\ContentBundle\Traits\DeletableTrait;
use TeamRock\ContentBundle\Traits\ViewableTrait;
use TeamRock\FauxBundle\Event\NewsViewEvent;


/**
 * News
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TeamRock\ContentBundle\Repository\NewsRepository")
 */
class News implements ViewableInterface, DeletableInterface
{
    /**
     * @var guid
     * @ORM\Column(name="identifier", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $identifier;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1)
     */
    protected $weight;

    /**
     * @var datetime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt=null;

    /*
     * Traits
     */
    use ViewableTrait;
    use DeletableTrait;

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        // not the best place to add this, could be pulled out into a class or abstracted
        // I wasn't sure of how you'd want it done
        $weightArray=array(''=>'Unspecified','i'=>'Important','s' => 'Semi Important','u' => 'Unimportant');
        return $weightArray[$this->weight];
    }

    /**
     * Set weight
     *
     * @param string $weight
     * @return News
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }    

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeletedAt()
    {

        return $this->deletedAt;
    }

    /**
     * Set deleted
     *
     * @param datetime $deletedAt
     * @return News
     */
    public function setDeletedAt()
    {
        $this->deletedAt = new \DateTime('now');

        return $this;
    }  

    public function onNewsViewEvent(NewsViewEvent $event) {
        $event->increment_views($this);
    }

}
