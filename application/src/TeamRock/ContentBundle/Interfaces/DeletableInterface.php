<?php

namespace TeamRock\ContentBundle\Interfaces;

interface DeletableInterface
{
    function softDelete();
    function hardDelete();
}
