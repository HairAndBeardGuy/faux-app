<?php

namespace TeamRock\ContentBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * NewsRepository
 */
class NewsRepository extends EntityRepository
{
    public function findSortedByViews($direction)
    {
        return $this->findBy(['deletedAt' => null], [
                'views' => $direction
            ]);
    }
}
