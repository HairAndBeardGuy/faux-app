<?php

namespace TeamRock\FauxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use TeamRock\ContentBundle\Entity\News;
use TeamRock\ContentBundle\Form\NewsAddType;
use TeamRock\FauxBundle\Event\NewsViewEvent;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        return [ ];
    }

    /**
     * @Route("/list/{identifier}", defaults = {"identifier" : "desc"}, name = "news_list")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function listNewsAction(Request $request, $identifier)
    {
        $newsRepository = $this->getDoctrine()->getRepository("TeamRockContentBundle:News");
        $news = $newsRepository->findSortedByViews($identifier); // updated to reflect the sorting by views

        return [ 'news' => $news ];
    }

    /**
     * @Route("/add", name="news_add")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function addNewsAction(Request $request)
    {
        $news = new News();
        $newsType = new NewsAddType();

        $newsForm = $this->createForm($newsType, $news);
        $newsForm->handleRequest($request);

        if ($newsForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('news_list'));
        }

        return [ 'newsForm' => $newsForm->createView() ];
    }

    /**
     * @Route("/view/{identifier})", name="news_view")
     * @Template()
     */
    public function viewNewsAction($identifier)
    {
        $news = $this->getDoctrine()->getRepository("TeamRockContentBundle:News")->find($identifier);

        $news->onView(); // to replace with the event dispatcher


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($news);
        $entityManager->flush();

        // code for the event dispatcher - not yet working
        /*
        $event=new NewsViewEvent();
        $dispatcher=$this->get('event_dispatcher');
        $dispatcher->dispatch('teamrock.events.newsview',$event);
        */
        return [ 'news' => $news ];
    }

    /**
     * @Route("/delete/{identifier}/{deleteType}", name="news_delete", defaults={"deleteType":"soft"})
     * @Template()
     */
    public function deleteNewsAction($identifier, $deleteType)
    {   

        $news = $this->getDoctrine()->getRepository("TeamRockContentBundle:News")->findOneBy(array('identifier'=>$identifier));

            if (!$news) {
                throw $this->createNotFoundException('Unable to find entry.');
            }
            $entityManager = $this->getDoctrine()->getManager();
            //$entityManager->remove($news);
                if($deleteType=="soft")
                {
                    $news->softDelete();
                }
                else if($deleteType=="hard")
                    {
                        // this does not properly implement the hardDelete() method
                        // I think I'd have to create an event listener that would trigger
                        // the removal of the entry from the database via the EntityManager
                        // which is not accessible from within the DeleteableTrait Method.
                        $entityManager->remove($news);
                        //$news->hardDelete();
                    }

            $entityManager->flush();

         return $this->redirect($this->generateUrl('news_list'));

    }
}
