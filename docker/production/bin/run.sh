#!/bin/bash
#
cd /var/www
php app/console doctrine:migrations:migrate --env=prod --no-interaction

/usr/sbin/apache2 -DFOREGROUND
